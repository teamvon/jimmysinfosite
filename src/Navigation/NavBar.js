import React, { Component } from 'react';
import { Menu, Icon } from 'antd';


class NavBar extends Component {

    state = {
        selected: this.props.selected,
    }

    changePage = (page) => {
        this.setState({
            selected: page
        })
        this.props.changeIndex(page);
    }

    render() {
        return (
            <Menu
                theme="dark"
                mode="horizontal"
                style={{ lineHeight: '64px' }}
            >
            <Menu.Item onClick={() => { this.changePage(1) }}>
                Home
            </Menu.Item>
            <Menu.Item onClick={() => { this.changePage(3) }}>
                Past Projects
            </Menu.Item>
            <Menu.Item onClick={() => { this.changePage(2) }}>
                About Us
            </Menu.Item>
            <Menu.Item onClick={() => { this.changePage(4) }}>
                Add Users
            </Menu.Item>
            </Menu>
        );
    }
}

export default NavBar;
