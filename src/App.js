import React, { Component } from 'react';
import 'antd/dist/antd.css';
import NavBar from './Navigation/NavBar'
import AboutUs from './Content/AboutUs';
import Home from './Content/Home'
import PastProjects from './Content/PastProjects'
import { Button, Layout } from 'antd'
import AddUsersPage from './AddUsers/AddUsersPage'

const HOMEPAGE = 1;
const ABOUTUSPAGE = 2;
const PASTPROJECTSPAGE = 3;
const ADDUSERSPAGE = 4;

const { Header, Content, Footer } = Layout;
class App extends Component {
  state = {
    pageIndex: ADDUSERSPAGE,
    visible: false
  }


  changeIndex = (index) => {
    this.setState({
      pageIndex: index
    })
  }

  displayClickedIndex = (index) => {
    alert(index);
  }

  render() {
    var page;
    var index = this.state.pageIndex;

    if (index == HOMEPAGE) {
      page = <Home />
    }
    else if (index == ABOUTUSPAGE) {
      page = <AboutUs />
    }
    else if (index == PASTPROJECTSPAGE) {
      page = <PastProjects />
    }
    else if (index == ADDUSERSPAGE) {
      page = <AddUsersPage />
    }
    return (
      <div className="App">
        <Layout>
          <NavBar changeIndex={this.changeIndex.bind(this)} />
          
          <Content style={{ background: '#fff', padding: 24 }} >
            <Row>
              <Col style={{ background: "red" }} xs={0} sm={2}></Col>
              <Col xs={24} sm={20}>
                {page}
              </Col>
              <Col style={{ background: "red" }} xs={0} sm={2}></Col>
            </Row>
          </Content>

          {/* <Footer style={{ textAlign: 'center', bottom: 0, position: "fixed", width: "100%" }}>
            footer stuff
          </Footer> */}

        </Layout>


      </div>
    );
  }
}

export default App;
