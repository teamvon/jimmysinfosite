import React, { Component } from 'react';
import { List } from 'antd'

class UsersList extends Component {

    state = {
        users: this.props.users
    }

    componentWillReceiveProps(newProps){
        if(this.state.users != newProps.users){
            this.setState({
                users: newProps.users
            })
        }
    }

    removeUser = (id) =>{
        this.props.removeUserById(id);
    }

    render() {
        return (
            <List
                dataSource={this.state.users}
                renderItem={user => (
                    <List.Item>
                        Name: {user.name} - Age: {user.age} - {user.id} - 
                        <a onClick={() => { this.removeUser(user.id)}}>Remove</a>
                    </List.Item>
                )}
            >
            </List>
        );
    }
}

export default UsersList;
