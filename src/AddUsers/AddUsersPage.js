import React, { Component } from 'react';
import { Row, Col, Image, Input, Button } from 'antd'
import UsersList from './UsersList'
import uuid from 'uuid'

class AddUsersPage extends Component {

    state = {
        newUserName: "",
        users: [
            {
                name: "Nick",
                age: 22,
                id: uuid()
            },
            {
                name: "Ross",
                age: 19,
                id: uuid()
            },
            {
                name: "Ryan",
                age: 23,
                id: uuid()
            },
        ]
    }

    updateNewUserName = (e) => {
        this.setState({
            newUserName: e.target.value
        });
    }

    addUser = () => {
        var age = Math.floor((Math.random() * 30) + 19);

        var newUser = {
            name: this.state.newUserName,
            age: age,
            id: uuid()
        }
        var currentUsers = this.state.users;

        var allUsers = [...currentUsers, newUser];

        this.setState({
            users: allUsers
        });
    }
 
    removeUserById = (id) =>{
        var newArrayOfUsers = this.state.users.filter((object) => {
            return object.id != id;
        });

        this.setState({
            users: newArrayOfUsers
        })
    } 

    render() {
        return (
            <Row>
                <Col xs={0} sm={2}></Col>
                <Col xs={24} sm={20}>
                    <Row>
                        <Col xs={12}>
                            <Input
                                value={this.state.newUserName}
                                onChange={(e) => {this.updateNewUserName(e)}}
                            />
                        </Col>
                        <Col xs={12}>
                            <Button type="primary" onClick={() => {this.addUser()}}>Save</Button>
                        </Col>
                    </Row>
                    <Row>
                        <Col xs={24}>
                            <UsersList removeUserById={this.removeUserById.bind(this)} users={this.state.users} />
                        </Col>
                    </Row>
                </Col>
                <Col xs={0} sm={2}></Col>
            </Row>
        );
    }
}

export default AddUsersPage;
